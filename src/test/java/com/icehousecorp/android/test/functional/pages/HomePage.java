package com.icehousecorp.android.test.functional.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class HomePage extends PageObject {

    @FindBy(id = "com.example.android.architecture.blueprints.todomvvmlive.mock:id/fab_add_task")
    private WebElementFacade id_btn_add;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
    private WebElementFacade xpath_btn_menu;

    @FindBy(xpath = "//*[contains(@resource-id, \"design_navigation_view\")]/child::node()[3]")
    private WebElementFacade xpath_statictics;

    @FindBy(xpath = "com.example.android.architecture.blueprints.todomvvmlive.mock:id/tasks_list")
    private WebElementFacade xpath_tasklist;

    public void click_add() {
        id_btn_add.click();
    }

    public void click_btn_menu () {
        xpath_btn_menu.click();
    }

    public void click_menu_statistics () {
        xpath_statictics.click();
    }
    public WebElementFacade getXpath_tasklist(){
        return getXpath_tasklist();
    }

}
