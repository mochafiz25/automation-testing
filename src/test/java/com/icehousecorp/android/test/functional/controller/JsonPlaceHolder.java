package com.icehousecorp.android.test.functional.controller;

import com.icehousecorp.android.test.functional.Utils.Config;
import com.icehousecorp.android.test.functional.models.PostRequestBody;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import net.thucydides.core.annotations.Step;

import static io.restassured.config.EncoderConfig.encoderConfig;
import static net.serenitybdd.rest.SerenityRest.config;
import static net.serenitybdd.rest.SerenityRest.given;

public class JsonPlaceHolder {

    @Step
    public void getData() {

        String baseurl = "https://jsonplaceholder.typicode.com";

        Response result = given()
                .contentType(ContentType.JSON).log().all()
                .when()
                .get(baseurl + "/posts")
                .then()
                .statusCode(200).log().all().extract().response();

        System.out.println(result.body());
    }

    @Step
    public String getData1() {

        String baseurl = "https://jsonplaceholder.typicode.com";

        Response result = given()
                .contentType(ContentType.JSON).log().all()
                .when()
                .get(baseurl + "/posts")
                .then()
                .statusCode(200).log().all().extract().response();

        System.out.println(result.body());

        return result.body().toString();
    }

    @Step
    public void postData() {

        String baseurl = "https://jsonplaceholder.typicode.com";

//        String body = "{\n" +
//                "      \"title\": \"foo\",\n" +
//                "      \"body\": \"bar\",\n" +
//                "      \"userId\": 1\n" +
//                "    }";

        PostRequestBody body = PostRequestBody.builder()
                .title("foo")
                .body("bar")
                .userId(1)
                .build();

        Response result = given()
                .contentType(ContentType.JSON).log().all()
                .body(body)
                .when()
                .post(baseurl + "/posts")
                .then()
                .statusCode(201).log().all().extract().response();
    }

    @Step
    public void post_signin_session(String username, String password) {
        String request = "https://qa-mybb-gw.bluebirdgroup.com:7443/";

        String endpoint = "api/v4/sessions";

        String body = "{\n" +
                "  \"email\": \"" + username + "\",\n" +
                "  \"password\": \"" + password + "\",\n" +
                "  \"uuid\": \"145c59c2-1d0d-4e39-a2ae-7f72dfc08066\",\n" +
                "  \"operating_system\": \"android\",\n" +
                "  \"manufacturer\": \"motorola\",\n" +
                "  \"os_version\": \"5.1\",\n" +
                "  \"device_token\": \"vvvvvv\"\n" +
                "}";

        Response result = given()
                .config(config().encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
                .contentType(ContentType.JSON).log().all()
                .body(body)
                .when()
                .post(request + endpoint)
                .then().statusCode(200).log().all()
                .extract().response();

        System.out.println("--------------");
        System.out.println("\nReponse body : " +result.body().asString());
        System.out.println("\nHeader" + result.getHeader("Content-Type"));
        System.out.println("\nData Token : " + result.path("data.token").toString());

        Config.saveTempJson(result.body().asString(), "Login.json");

    }
}
