package com.icehousecorp.android.test.functional.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class NewTodoPage extends PageObject {

    @FindBy(id = "add_task_title")
    private WebElementFacade id_textfield_title;

    @FindBy(id = "add_task_description")
    private WebElementFacade id_textfield_description;

    @FindBy(id = "fab_edit_task_done")
    private WebElementFacade id_btn_submit;

    public void type_textfield_title (String text) {
        id_textfield_title.type(text);
    }

    public void type_textfield_description (String text) {
        id_textfield_description.type(text);
    }

    public void click_btn_submit () {
        System.out.printf("TestX :" + id_btn_submit.getLocation().getX());
        System.out.printf("TestY :" + id_btn_submit.getLocation().getY());
        System.out.printf("High :" + id_btn_submit.getSize().getHeight());
        System.out.printf("Width :" + id_btn_submit.getSize().getWidth());
        id_btn_submit.click();
    }
}
