package com.icehousecorp.android.test.functional.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PostRequestBody {

    String title;

    String  body;

    Integer userId;
}
