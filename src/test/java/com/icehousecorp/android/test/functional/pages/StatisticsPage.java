package com.icehousecorp.android.test.functional.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class StatisticsPage extends PageObject {

    @FindBy(xpath = "//*[contains(@resource-id, \"statistics\")]//*[@index=\"0\"]")
    private WebElementFacade xpath_active_task;

    public String get_text_activeTask() {
        return xpath_active_task.getText();
    }
}
