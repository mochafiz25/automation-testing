package com.icehousecorp.android.test.functional.behaviour;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icehousecorp.android.test.functional.AndroidDriverSource;
import com.icehousecorp.android.test.functional.models.DataObject;
import com.icehousecorp.android.test.functional.models.LoginEntity;
import com.icehousecorp.android.test.functional.pages.HomePage;
import com.icehousecorp.android.test.functional.pages.NewTodoPage;
import com.icehousecorp.android.test.functional.pages.StatisticsPage;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

import java.io.FileReader;

import static com.icehousecorp.android.test.functional.AndroidDriverSource.ANDROID_DRIVER;
import static org.hamcrest.Matchers.equalTo;

import static org.hamcrest.MatcherAssert.assertThat;

public class AddTodo {

    HomePage home;
    NewTodoPage newTodo;
    StatisticsPage sta;

    @Step
    public void openToDo() {
        home.setDriver(home.getDriver());
        System.out.println(ANDROID_DRIVER);
    }

    @Step
    public void addNewTodo() {
        home.click_add();
    }

    @Step
    public void typeTitle(String title) {
        newTodo.type_textfield_title(title);
    }

    @Step
    public void typeDesc(String desc) {
        newTodo.type_textfield_description(desc);
    }

    @Step
    public void AddTodoSubmit() {
        newTodo.click_btn_submit();
    }

    @Step
    public void clickMenu() {
        home.click_btn_menu();
    }

    @Step
    public void clickMenuStatistics() {
        home.click_menu_statistics();
    }

    @Step
    public void checkActivityTask(Integer activity) {
        assertThat("Active task is not as expected", sta.get_text_activeTask(), equalTo("Active tasks: " + activity));
    }

    public void getToken() throws Exception {
        DataObject<LoginEntity> data = new Gson().fromJson(new FileReader(System.getProperty("user.dir") + "//target//Login.json"), new TypeToken<DataObject<LoginEntity>>() {}.getType());
        System.out.println("Data Token : " + data.getData().getFirebaseToken());
        System.out.println("Privileges : " + data.getData().getPrivileges().get(2));
    }
//
//    public void tapByCoordinate(int x, int y) {
//        PointOption pointOption = new PointOption();
//        pointOption.withCoordinates(x, y);
//        new TouchAction(ANDROID_DRIVER).tap(pointOption).perform();
//    }
//
//    public void moveToElement() {
//        PointOption pContainer = new PointOption();
//        PointOption pTarget = new PointOption();
//        pContainer.withCoordinates(500, 1700);
//        pTarget.withCoordinates(500,500);
//        new TouchAction(ANDROID_DRIVER).longPress(pTarget).moveTo(pContainer).release().perform();
//
//    }

}
