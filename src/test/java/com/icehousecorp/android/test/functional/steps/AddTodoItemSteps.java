package com.icehousecorp.android.test.functional.steps;

import com.icehousecorp.android.test.functional.behaviour.AddTodo;
import com.icehousecorp.android.test.functional.controller.JsonPlaceHolder;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AddTodoItemSteps {

    @Steps
    AddTodo todo;

    @Steps
    JsonPlaceHolder api;

    @Given("^the user to do list screen$")
    public void theUserTodolistScreen() {
        todo.openToDo();
    }

    @When("^(?:the user|the customer) add new to do with title ([^\"]*) and description \"([^\"]*)\"$")
    public void theUserAddNewToDoWithTitleAndDescription(String arg0, String arg1) throws Exception {
        api.post_signin_session("ih0@icehouse.com", "password");
        todo.addNewTodo();
        todo.typeTitle(arg0);
        todo.typeDesc(arg1);
        todo.AddTodoSubmit();
        todo.getToken();
//        todo.moveToElement();
    }

    @Then("^Active Task on Statistics menu should be (\\d+)$")
    public void activeTaskOnStatisticsMenuShouldBe(int arg0) {
        todo.clickMenu();
        todo.clickMenuStatistics();
        todo.checkActivityTask(arg0);
    }

    @When("^user want to get some info$")
    public void userWantToGetSomeInfo() {
        api.getData();
    }

    @And("^user want to input data$")
    public void userWantToInputData() {
        api.postData();
    }

//    @When("^the user check to do list$")
//    public void theUserCheckToDoList() {
//        todo.tapByCoordinate(48,694);
//    }
}
