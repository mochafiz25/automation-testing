@Regression
Feature: Add a TODO item
  In order to remind things To do
  As a user
  I want to add to do item

  Scenario Outline: Add a ToDo item 'Training Class'
    Given the user to do list screen
    When user want to get some info
    And user want to input data
    When the user add new to do with title <Title> and description "<Description>"
    Then Active Task on Statistics menu should be <Active>
    When the user check to do list

    Examples:
      | Title      | Description               | Active |
      | Training 1 | Create new cucumber story | 1      |
#      | Training 2 | Create new cucumber story | 2      |
#      | Training 3 | Create new cucumber story | 3      |
